export declare class UserDTO {
    readonly name: string;
    readonly gender: string;
    readonly age: number;
    readonly email: string;
    readonly phone: number;
    readonly seatnumber: number;
}
