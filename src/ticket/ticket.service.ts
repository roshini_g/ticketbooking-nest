import { Body, Injectable, NotFoundException} from '@nestjs/common';
import { debug } from 'console';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {User} from './interfaces/user.interface';
import {UserDTO} from'./dto/user.dto';
import {Ticket} from './interfaces/ticket.interface';
import {TicketDTO} from'./dto/ticket.dto';
@Injectable()
export class TicketService {
    constructor(@InjectModel('Ticket') private readonly ticketModel:Model<Ticket>,
    @InjectModel('User') private readonly userModel:Model<User>){}
async addUser(userDTO:UserDTO):Promise<User>{
    const newUser= await new this.userModel(userDTO);
    const seat=userDTO.seatnumber;
    if(await this.ticketModel.findOne({seatnumber:seat}))
            throw console.error("Seat already booked");
        else{  
        const ticket=this.ticketModel.insertMany({seatnumber:seat,isbooked:true});
        return newUser.save();
        }
 }
 /*async getTicket(ticketid:string){
     const ticket=await this.findTicket(ticketid);
     return{
         seatnumber:ticket.seatnumber,
         isbooked:ticket.isbooked,
     };
 }
 async getTicketDetails(ticketid:string){
    const ticket=await this.findTicket(ticketid);
    let seat=ticket.seatnumber;
    return await this.userModel.find({seatnumber:seat})
 }
async findTicket(ticketid:string):Promise<Ticket>{
    let ticket;
    try{
        ticket=await this.ticketModel.findById(ticketid).exec();
    }catch(error){
        throw new NotFoundException('Could not find Ticket');
    }
    if(!ticket){
        throw new NotFoundException('Could not find ticket');
    }
    return ticket;
}*/

async openTicket(){
    return await this.ticketModel.find({isbooked:false});
}
async closeTicket(){
    return await this.ticketModel.find({isbooked:true});
}

async resetTickets(ticketDTO:TicketDTO):Promise<Ticket>{
    return await this.ticketModel.updateMany({},{$set: {isbooked:false}})
}
async findById(ID: number): Promise<Ticket> {
    const ticket= await this.ticketModel.findById(ID).exec();
    return ticket;
}

async findDetailsById(ID:number){
    const passenger=await this.findById(ID);
    let seat=passenger.seatnumber;
    return await this.userModel.find({seatnumber:seat})
}

async updateTicketStatus(ticketID: string, ticketDTO: TicketDTO): Promise<Ticket> {
    const updatedTicket = await this.ticketModel
                        .findByIdAndUpdate(ticketID, ticketDTO, {new: true});
    return updatedTicket;
}











}
 

