import { Body, Controller,HttpStatus,Post, Res,Param, NotFoundException, Get,Put,Query} from '@nestjs/common';
import{TicketService}from'./ticket.service';
import{UserDTO} from './dto/user.dto';
import{TicketDTO} from './dto/ticket.dto';

@Controller('ticket')
export class TicketController {
constructor(private ticketService:TicketService) {}
@Post('/create')
async addTicket(@Res() res, @Body() userDTO:UserDTO /*,ticketDTO:TicketDTO*/)
{
    const newUser=await this.ticketService.addUser(userDTO);
    return res.status(HttpStatus.OK).json({
        message: 'Ticket booked successfully!',
        post:newUser
      });
}
/*@Get('/:ticketid')
async getTicket(@Param('ticketid')ticketid:string){
    const ticket= await this.ticketService.getTicket(ticketid);
    return ticket;
}
@Get('/details/:ticketid')
async getTicketDetails(@Param('ticketid')ticketid:string){
    const ticketdetails=await this.ticketService.getTicketDetails(ticketid);
    return ticketdetails;
}*/
@Get('/status/open')
async openTicket(){
    const open=await this.ticketService.openTicket();
    console.log(open);
}
@Get('/status/close')
async closeTicket(){
    const close=await this.ticketService.closeTicket();
    console.log(close);
}

@Post('/reset')
async resetTickets(@Res() res ,@Body() body,ticketDTO:TicketDTO){
    const username="Admin";
    const password="123456";
    if(body.username==username && body.password==password){
        const reset=await this.ticketService.resetTickets(ticketDTO);
        return res.status(HttpStatus.OK).json({
        message: 'All tickets has been reset!!'
  });
}else{res.send("Invalid login credentials!!")}
}
@Get('/:id')
    public async getTicketid(@Res() res, @Param() param){
        const ticket = await this.ticketService.findById(param.id);
        return res.status(HttpStatus.OK).json(ticket);
    }
@Get('/details/:id')
public async getTicketdetails(@Res() res, @Param() param){
    const ticket = await this.ticketService.findDetailsById(param.id);
        return res.status(HttpStatus.OK).json(ticket);
    }

@Put('/update')//update ticket
async updateTicket(@Res() res, @Body() ticketDTO: TicketDTO, @Query('ticketID') ticketID) {
    const updatedTicket = await this.ticketService.updateTicketStatus(ticketID, ticketDTO);
    if (!updatedTicket) throw new NotFoundException('Ticket does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Ticket Updated Successfully',
            updatedTicket 
        });
    }













}



